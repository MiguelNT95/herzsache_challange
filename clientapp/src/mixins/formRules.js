export default {
  data () {
    return {
      rules: {
        required: (value) => !!value || 'Valor obrigatório.',
        positiveNumber: (value) => (!!value && Number(value) && Number(value) > 0) || 'Número deve ser positivo.'
      }
    }
  }
}
