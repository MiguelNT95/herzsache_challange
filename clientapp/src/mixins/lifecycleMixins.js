export default {
  methods: {
    toast (context, message) {
      this.$store.dispatch('toast', {
        context: context,
        message: context === 'error' ? 'Error: ' + message : message
      })
    },
    goBack () {
      this.$router.go(-1)
    },
    managerName (id) {
      const managers = this.$store.state.managers || []
      const manager = managers.find(el => el.id === id) || {}
      return manager.name || ''
    }
  },
  computed: {
    role () {
      let role = this.$store.getters.role
      if (!role) {
        const claims = JSON.parse(window.localStorage.getItem('user_claims'))
        if (claims) {
          this.$store.dispatch('setClaims', claims)
        }

        role = this.$store.getters.role
      }

      return role
    },
    technicianId () {
      const id = this.$store.getters.technicianId

      return id
    },
    isAdmin () {
      return this.role === 'Admin'
    },
    userId () {
      let userId = this.$store.getters.userId
      if (!userId) {
        const claims = JSON.parse(window.localStorage.getItem('user_claims'))
        if (claims) {
          this.$store.dispatch('setClaims', claims)
        }

        userId = this.$store.getters.userId
      }

      return userId
    },
    userName () {
      return 'Not implemented'
    }
  }
}
