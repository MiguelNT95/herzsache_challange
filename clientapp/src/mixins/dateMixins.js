export default {
  methods: {
    formatTime (duration) {
      if (duration <= 0) {
        return '00:00'
      }

      const minutes = parseInt((duration / 60000) % 60) || 0
      const hours = parseInt((duration / 360000) % 24) || 0

      return this.addZero(hours) + ':' + this.addZero(minutes)
    },
    formatTimeString (duration) {
      const durationParts = this.duration.split(':')
      const minutes = parseInt(durationParts[1])
      const hours = parseInt(durationParts[0]) * 60

      return (hours + minutes)
    },
    roundDate (date) {
      var coeff = 60000 // By the minute

      return new Date(Math.round(date.getTime() / coeff) * coeff)
    }
  }
}
