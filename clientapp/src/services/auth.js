// Adapted from: https://github.com/damienbod/IdentityServer4VueJs/blob/master/VueJsOidcClient/vue-js-oidc-client/src/services/auth.service.ts
import { Log, UserManager, WebStorageStateStore } from 'oidc-client'

export default class AuthService {
  constructor () {
    const STS_DOMAIN = process.env.VUE_APP_API_ENDPOINT
    const settings = {
      userStore: new WebStorageStateStore({ store: window.localStorage }),
      authority: STS_DOMAIN,
      client_id: 'VueTest.ClientApp',
      redirect_uri: `${location.origin}/callback.html`,
      post_logout_redirect_uri: `${location.origin}`,
      automaticSilentRenew: true,
      silent_redirect_uri: `${location.origin}/silent-renew.html`,
      response_type: 'code',
      scope: 'openid profile technician offline_access',
      filterProtocolClaims: true,
      // will revoke (reference) access tokens at logout time
      revokeAccessTokenOnSignout: true,
      // this will toggle if profile endpoint is used
      loadUserInfo: true
    }
    this.userManager = new UserManager(settings)

    if (process.env.NODE_ENV === 'development') {
      this.userManager.events.addAccessTokenExpiring(() => {
        console.log('AccessToken Expiring')

        this.userManager.signinSilent().then(user => {
          console.log('silent renew success', user)
        }).catch(err => {
          console.log('silent renew error', err.message)
        })
      })
      this.userManager.events.addAccessTokenExpired(() => {
        console.log('AccessToken Expired')

        this.userManager.signinSilent().then(user => {
          console.log('silent renew success', user)
        }).catch(err => {
          console.log('silent renew error', err.message)
        })
      })
      this.userManager.events.addSilentRenewError(() => {
        console.error('Silent Renew Error：', arguments)
      })

      Log.logger = console
      Log.level = Log.INFO
    }
  }

  // Get the user who is logged in
  getUser () {
    const self = this
    return new Promise((resolve, reject) => {
      self.userManager.getUser().then(function (user) {
        if (user == null) {
          self.login()
          return resolve(null)
        } else {
          return resolve(user)
        }
      }).catch(function (err) {
        console.log(err)
        return reject(err)
      })
    })
  }

  login () {
    this.userManager.signinRedirect().catch(err => {
      console.log(err)
    })
  }

  logout () {
    this.userManager.signoutRedirect().then(response => {
      console.log('signed out', response)
    }).catch(err => {
      console.log(err)
    })
  }

  // Check if there is any user logged in
  getLoggedIn () {
    const self = this
    return new Promise((resolve, reject) => {
      self.userManager.getUser().then(user => {
        if (user == null) {
          self.login()
          return resolve(false)
        } else {
          return resolve(true)
        }
      }).catch(err => {
        console.log(err)
        return reject(err)
      })
    })
  }

  // Get the access token of the logged in user
  getAccessToken () {
    const self = this
    return new Promise((resolve, reject) => {
      self.userManager.getUser().then(user => {
        if (user == null) {
          self.login()
          return resolve(null)
        } else {
          return resolve(user.access_token)
        }
      }).catch(err => {
        console.log(err)
        return reject(err)
      })
    })
  }

  // Renew the token manually
  renewToken () {
    const self = this
    return new Promise((resolve, reject) => {
      self.userManager.signinSilent().then(function (user) {
        if (user == null) {
          self.signIn(null)
        } else {
          return resolve(user)
        }
      }).catch(function (err) {
        console.log(err)
        return reject(err)
      })
    })
  }
}
