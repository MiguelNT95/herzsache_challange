import api from './api'

export default {
  getInterventions: function () {
    return api.call('/api/interventions')
  },
  findInterventions: function (location, page, pageSize, sort, search, includePending) {
    const params = {
      locationId: location,
      page: page,
      pageSize: pageSize
    }

    if (includePending) {
      params.includePending = includePending
    }
    if (sort) {
      params.sort = sort
    }
    if (search) {
      params.search = search
    }
    return api.call('/api/interventions/find', {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
      params: params
    })
  },
  getIntervention: function (id) {
    return api.call(`/api/interventions/${id}`)
  },
  createIntervention (intervention) {
    return api.call('/api/interventions/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(intervention)
    })
  },
  createReport (interventionId) {
    return api.call(`/api/interventions/${interventionId}/reports`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' }
    })
  },
  updateReport: function (report, status) {
    var formData = new FormData()
    formData.append('model', JSON.stringify(report))

    return api.call(`/api/interventions/${report.interventionId}/reports/${report.id}/${status}`, {
      method: 'PUT',
      headers: { Accept: 'application/json' },
      body: formData
    })
  },
  getInterventionReport: function (interventionId, reportId) {
    return api.call(`/api/interventions/${interventionId}/reports/${reportId}`)
  }
}
