import AuthService from './auth'

const auth = new AuthService()

let currentUser = null

// create a fetch request with url and props passed and return a promise with the data returned
// also add the auth header
function getFetchRequestPromise (endpoint, fetchProps) {
  fetchProps = fetchProps || {}
  fetchProps.headers = fetchProps.headers || {}

  const props = {}
  Object.keys(fetchProps).forEach(key => {
    if (key !== 'params' && key !== 'response') {
      props[key] = fetchProps[key]
    }
  })

  if (process.env.VUE_APP_ENABLE_CORS) {
    props.mode = 'cors'
  }

  return auth.getAccessToken().then(token => {
    props.headers.Authorization = 'Bearer ' + token

    return new Promise((resolve, reject) => {
      const url = new URL(process.env.VUE_APP_API_ENDPOINT + endpoint)

      if (fetchProps.params) {
        Object.keys(fetchProps.params).forEach(key => url.searchParams.append(key, fetchProps.params[key]))
      }

      return window.fetch(url, props).then((response) => {
        if (response.status >= 400) {
          return response.text().then(text => {
            console.log(text || response.statusText)
            reject(text || response.statusText)
          })
        } else {
          if (fetchProps.response && fetchProps.response === 'blob') {
            response.headers.forEach((val, key) => {
              console.log(key, val)
            })

            return response.blob().then(blob => {
              const header = response.headers.get('Content-Disposition')
              const filename = (header && header.match(/filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/)[1]) || ''

              return {
                stream: blob,
                name: filename
              }
            }).catch(err => {}) // eslint-disable-line handle-callback-err
          } else {
            return response.json().then(json => json).catch(err => {}) // eslint-disable-line handle-callback-err
          }
        }
      }).then((data) => {
        resolve(data)
      }).catch((error) => {
        reject(error.message) // TODO sanitize error
      })
    })
  })
}

export default {
  call: getFetchRequestPromise,
  getUsername () {
    if (currentUser) {
      return currentUser.profile.name
    } else {
      return auth.getUser().then(user => {
        currentUser = user
        return currentUser.profile.name
      })
    }
  },
  login () {
    auth.login()
  },
  isLoggedIn () {
    auth.getLoggedIn()
  },
  logout () {
    auth.logout().then(() => {
      currentUser = null
    })
  },
  getUserProfile: function () {
    if (currentUser) {
      return currentUser.profile
    } else {
      return auth.getUser().then(user => {
        currentUser = user
        return currentUser.profile
      })
    }
  },
  getTechnician: function (userId) {
    return getFetchRequestPromise(`/api/technicians/userid/${userId}`)
  },
  getManagers: function () {
    return getFetchRequestPromise('/api/managers')
  },
  getMaintenanceStatuses: function () {
    return getFetchRequestPromise('/api/maintenancestatuses')
  },
  getMaintenancePriorities: function () {
    return getFetchRequestPromise('/api/maintenancepriorities')
  },
  getLocations: function () {
    return getFetchRequestPromise('/api/locations')
  }
}
