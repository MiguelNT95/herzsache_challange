import api from './api'

export default {
  getMaterials: function () {
    return api.call('/api/materials')
  }
}
