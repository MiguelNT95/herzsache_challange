const state = {
  userInfo: {}
}

const getters = {}

const actions = {
  getUserInfo () {
    fetch('http://localhost:8081/MOCK_DATA.json', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => {
        return response.json()
      })
      .then(
        body => {
          console.log(body)
        },
        error => {
          console.log(error)
        }
      )
  }
}

const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
