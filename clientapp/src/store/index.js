import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const vuexLocalStorage = new VuexPersist({
  key: 'vuex', // The key to store the state on in the storage provider.
  storage: window.localStorage // or window.sessionStorage or localForage
  // Function that passes the state and returns the state with only the objects you want to store.
  // reducer: state => state,
  // Function that passes a mutation and lets you decide if it should update the state in localStorage.
  // filter: mutation => (true)
})

// TYPES

// STATE
const state = {
  drawer: false,
  user: {},
  toast: {},
  toastVisible: false,
  maintenanceStatuses: [],
  maintenancePriorities: [],
  locations: [],
  managers: [],
  currentScreenCache: {},
  interventionsHistory: []
}

// MUTATIONS
const mutations = {
  drawer (state, mini) {
    state.drawer = mini
  },
  toast (state, toast) {
    state.toast = toast
  },
  toastVisible (state, visible) {
    state.toastVisible = visible
  },
  userId (state, id) {
    Vue.set(state.user, 'id', id)
  },
  userName (state, name) {
    Vue.set(state.user, 'name', name)
  },
  userRole (state, role) {
    Vue.set(state.user, 'role', role)
  },
  technicianId (state, id) {
    Vue.set(state.user, 'technicianId', id)
  },
  cacheMaintenanceStatuses (state, statuses) {
    state.maintenanceStatuses = statuses
  },
  cacheMaintenancePriorities (state, priorities) {
    state.maintenancePriorities = priorities
  },
  cacheLocations (state, locations) {
    state.locations = locations
  },
  cacheManagers (state, managers) {
    state.managers = managers
  },
  cacheScreen (state, data) {
    state.currentScreenCache = data
  },
  cacheInterventionsHistory (state, interventionsHistory) {
    state.interventionsHistory = interventionsHistory
  }
}

// ACTIONS
const actions = {
  logout (state) {
    store.commit('toast', {})
    store.commit('toastVisible', false)
    store.commit('userId')
    store.commit('userName')
    store.commit('userRole')
    store.commit('technicianId')
    store.commit('cacheMaintenanceStatuses')
    store.commit('cacheMaintenancePriorities')
    store.commit('cacheLocations')
    store.commit('cacheManagers')
    store.commit('cacheScreen')
    store.commit('cacheInterventionsHistory')
    window.localStorage.removeItem('user_claims')
  },
  toast (store, toast) {
    store.commit('toast', toast)
    store.commit('toastVisible', true)
  },
  setClaims (store, claims) {
    store.commit('userId', claims.sub || '')
    store.commit('userName', claims.name || '')
    store.commit('userRole', claims.role || '')
    store.commit('technicianId', claims.techId || '')

    window.localStorage.setItem('user_claims', JSON.stringify(claims))
  }
}

// GETTERS
const getters = {
  role (state) {
    return state.user && state.user.role
  },
  userId (state) {
    return state.user && state.user.id
  },
  userName (state) {
    return state.user && state.user.name
  },
  technicianId (state) {
    return state.user && state.user.technicianId
  }
}

const store = new Vuex.Store({
  // root state object.
  // each Vuex instance is just a single state tree.
  state,
  // mutations are operations that actually mutates the state.
  // each mutation handler gets the entire state tree as the
  // first argument, followed by additional payload arguments.
  // mutations must be synchronous and can be recorded by plugins
  // for debugging purposes.
  mutations,
  // actions are functions that cause side effects and can involve
  // asynchronous operations.
  actions,
  // getters are functions
  getters,
  plugins: [vuexLocalStorage.plugin]
})

export default store
