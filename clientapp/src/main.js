import Vue from 'vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'

import vuetify from './plugins/vuetify'

import App from './components/App'

import './main.styl'

Vue.config.productionTip = false

sync(store, router)

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')

// export {
//   app,
//   router,
//   store
// }
