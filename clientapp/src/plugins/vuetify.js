import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import DatetimePicker from 'vuetify-datetime-picker'

Vue.use(Vuetify)
Vue.use(DatetimePicker)

// https://vuetifyjs.com/en/getting-started/releases-and-migrations/#releases-and-migrations
export default new Vuetify({
  icons: {
    iconfont: 'mdi'
  },
  theme: {
    themes: {
      light: {
        primary: '#98dd22',
        secondary: '#4CAF50',
        error: '#b71c1c'
      },
      dark: {
        primary: '#98dd22',
        secondary: '#4CAF50',
        error: '#b71c1c'
      }
    }
  }
})
