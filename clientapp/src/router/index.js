import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '@/store'
import { routes } from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
  hashbang: false,
  mode: 'history',
  base: process.env.BASE_URL,
  saveScrollPosition: true,
  routes,
  scrollBehavior (to, from, savedPosition) {
    // Tell the route if we came from a history back. Allow to cache and restore screen data
    to.meta.fromHistory = savedPosition !== null

    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  if (to.meta && to.meta.requiresAuth) {
    if (!to.meta.requiresRole || to.meta.requiresRole.indexOf(store.getters.role) !== -1) {
      next()
    } else if (!to.meta.requiresProp || store.getters[to.meta.requiresProp.name] === to.meta.requiresProp.value) {
      next()
    } else {
      next(Error('Unauthorized'))
    }
  } else {
    next()
  }
})

export default router
