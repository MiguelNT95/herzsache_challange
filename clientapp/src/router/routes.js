import Dashboard from '@/components/Dashboard'
import Intervention from '@/components/interventions/Intervention'
import InterventionPending from '@/components/interventions/InterventionPending'
import InterventionHistory from '@/components/interventions/InterventionHistory'
import InterventionReport from '@/components/interventions/InterventionReport'
import InterventionCreate from '@/components/interventions/InterventionCreate'
import ShowUsers from '@/components/User'

export const routes = [
  {
    path: '',
    name: 'dashboard',
    component: Dashboard,
    children: [
      {
        path: '/ShowUsers',
        name: 'ShowUsers',
        component: ShowUsers
      },
      {
        path: '/dashboard',
        name: 'interventions_pending',
        component: InterventionPending,
        meta: { requiresAuth: true }
      },
      {
        path: '/interventions/history',
        name: 'interventions_history',
        component: InterventionHistory,
        meta: { requiresAuth: true }
      },
      // Technicians
      {
        path: '/interventions',
        name: 'intervention_crate',
        component: InterventionCreate,
        meta: {
          requiresRole: 'Technician'
        }
      },
      {
        path: '/interventions/:interventionid/reports/:reportid/:context',
        name: 'interventionreport',
        component: InterventionReport,
        props: true,
        meta: {
          requiresRole: 'Technician'
        }
      },
      {
        path: '/interventions/:id/:context',
        name: 'intervention_edit',
        component: Intervention,
        props: true,
        meta: {
          requiresRole: 'Technician'
        }
      },
      {
        path: '/interventions/:id',
        name: 'intervention_read',
        component: Intervention,
        props: true,
        meta: {
          requiresRole: 'Technician'
        }
      }
    ]
  }
]
