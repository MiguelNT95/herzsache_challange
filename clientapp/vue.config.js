const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  configureWebpack: {
    plugins: [
      new CopyWebpackPlugin({
        patterns: [{
          from: 'node_modules/oidc-client/dist/oidc-client.min.js',
          to: 'js'
        }]
      })
    ]
  },
  devServer: 
  { 
    https: true,
    public : 'https://localhost:5001',
  },
  transpileDependencies: [
    'vuetify'
  ]
}
