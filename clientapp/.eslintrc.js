module.exports = {
  root: true,
  env: {
    node: true,
    browser: true,
    es6: true,
  },
  extends: [
    'plugin:vue/essential',
    'plugin:vue/base',
    'plugin:vuetify/base',
    '@vue/standard'
  ],
  plugins: [
    'vue',
    'vuetify'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    // https://github.com/vuetifyjs/eslint-plugin-vuetify
    'vuetify/no-deprecated-classes': 'error',
    'vuetify/grid-unknown-attributes': 'error',
    'vuetify/no-legacy-grid': 'error',
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off'
  }
}
